(function () {
  'use strict';

  angular
    .module('articles')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Feedback',
      state: 'articles',
      type: 'dropdown',
      roles: ['user']
    });

 // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'articles', {
      title: 'Send Feeback to',
      state: 'articles.list',
      roles: ['user']
    }); 

     // Add the dropdown list item
     menuService.addSubMenuItem('topbar', 'articles', {
      title: 'My Feedback',
      state: 'articles.mylist',
      roles: ['user']
    }); 
  }
}());
