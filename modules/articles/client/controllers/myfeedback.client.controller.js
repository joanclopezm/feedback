(function () {
  'use strict';

  angular
    .module('articles')
    .controller('MyfeedbackListController', ArticlesController);

  ArticlesController.$inject = ['$scope', 'Authentication','$state','Notification','$http'];

  function ArticlesController($scope, Authentication,$state,Notification, $http) {
    var vm = this;

    vm.authentication = Authentication;
    $http({
      method: 'GET',
      url: '/api/myfeedback'
    }).then(function successCallback(response) {
      console.log(response)
      vm.articles=response.data;
        // this callback will be called asynchronously
        // when the response is available
      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });

  }
}());
